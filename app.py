import flask
import collections
import requests
import decimal
import random

requests.packages.urllib3.disable_warnings()

from flask import Flask, request
import flickrapi
from flask.ext.cors import CORS
from skyscanner import skyscanner

app = Flask(__name__)
cors = CORS(app)

from amadeus.api import Transport, Flights, Hotels

FLICKR_KEY = 'b5333fff6b801a18f4b0d3b8866f30e9'
FLICKR_SECRET = 'db907c8f80e553a9'
# SKYSCANNER_API_KEY = 'prtl6749387986743898559646983194'
SKYSCANNER_API_KEY = 'ad348902349238472384234920940923'

flights = Flights()
hotels = Hotels()
transport = Transport()

@app.route("/flickr")
def flickr():
    flickr = flickrapi.FlickrAPI(FLICKR_KEY, FLICKR_SECRET)

    raw_json = flickr.photosets.getList(user_id='73509078@N00')

    print(raw_json)
    return raw_json

def _get_country(country_code):
    from amadeus.countries import COUNTRIES_MAP
    for country in COUNTRIES_MAP:
        if country['code'] == country_code:
            return country['name']

    return None

def _get_meal_cost(country):
    from amadeus.costs import MEAL_COST
    data = {}

    try:
        data.update({'cost': MEAL_COST[country]}) 
    except KeyError:
        cost = {
            'low': str(decimal.Decimal(random.randrange(1000, 1500))/100),
            'medium': str(decimal.Decimal(random.randrange(2000, 2500))/100),
            'high': str(decimal.Decimal(random.randrange(3000, 3500))/100)
        }
        data.update({
            'cost': cost
        })
    return data    

@app.route("/get-meal-cost")
def get_meal_cost():
    country_code = request.args.get('country_code', 'CN')
    country = _get_country(country_code)  
    print(country)  
    data = _get_meal_cost(country) 
    return flask.jsonify(**data)

def _get_travel_cost(country):
    from amadeus.costs import TRAVEL_COST
    data = {}

    try:
        data.update({'cost': TRAVEL_COST[country]}) 
    except KeyError:
        cost = {
            'low': str(decimal.Decimal(random.randrange(1000, 1500))/100),
            'medium': str(decimal.Decimal(random.randrange(2000, 2500))/100),
            'high': str(decimal.Decimal(random.randrange(3000, 3500))/100)
        }
        data.update({
            'cost': cost
        })

    return data

@app.route("/get-travel-cost")
def get_travel_cost():
    country_code = request.args.get('country_code', 'CN')
    country = _get_country(country_code)  
    print(country)

    data = _get_travel_cost(country)
    return flask.jsonify(**data)    

@app.route("/populate-cities")
def populate_cities():
    country_code = request.args.get('country_code', 'CN')
    country = _get_country(country_code)  
    print(country)  
    from amadeus.cities import COUNTRY_CITIES
    data = {}
    data.update({'cities': COUNTRY_CITIES[country]}) 

    return flask.jsonify(**data)


def _get_mapped_data(selected_id, data):
    for d in data:
        if d['Id'] == selected_id:
            return d
    return None

def _stitch_quotes(data):

    def _convert_unicode(dictionary):
        """Recursively converts dictionary keys to strings."""
        if not isinstance(dictionary, dict):
            return dictionary
        return dict((str(k), _convert_unicode(v)) 
            for k, v in dictionary.items()) 

    quotes = []
    for d in data['Itineraries']:
        quote = None
        quote = d
        # print(d)
        inbound_leg = _get_mapped_data(d['InboundLegId'], data['Legs'])
        outbound_leg = _get_mapped_data(d['OutboundLegId'], data['Legs'])

        inbound_segments = []
        for segment_id in inbound_leg['SegmentIds']:
            segment = _get_mapped_data(int(segment_id), data['Segments'])
            carrier = _get_mapped_data(segment['Carrier'], data['Carriers'])
            print("carrier: %s" % carrier)
            segment.update({'CarrierDetails': carrier})
            print("segment: %s" % segment)            
            inbound_segments.append(segment)

        outbound_segments = []
        for segment_id in outbound_leg['SegmentIds']:
            segment = _get_mapped_data(int(segment_id), data['Segments'])
            carrier = _get_mapped_data(segment['Carrier'], data['Carriers'])
            segment.update({'CarrierDetails': carrier})
            outbound_segments.append(segment)            

        inbound_leg.update({'Segments': inbound_segments})
        outbound_leg.update({'Segments': outbound_segments})
        # for carrier_id in inbound_leg['Carriers']:
        #     carrier = _get_mapped_data(int(carrier_id), data['Carriers'])
        #     print(carrier)

        # print("outbound_leg: %s" % outbound_leg)
        quote.update({'Outbound': outbound_leg})
        quote.update({'Inbound': inbound_leg})
        quotes.append(quote)

    return quotes

@app.route("/get-flights")
def get_flights():
    flights_service = skyscanner.Flights(SKYSCANNER_API_KEY, response_format='json')
    data = flights_service.get_result(
        errors='graceful',
        country='US',
        currency='USD',
        locale='en-US',
        originplace='SIN-sky',
        destinationplace='KUL-sky',
        outbounddate='2015-10-28',
        inbounddate='2015-10-30',
        adults=1).parsed 

    quotes = _stitch_quotes(data)
    updated_data = {}
    updated_data.update({'quotes': quotes})
    return flask.jsonify(**updated_data)

@app.route("/where-to-go")
def where_to_go():
    def _include_cost_data(data):
        updated_results = []
        print("_include_cost_data")

        for d in data['results']:
            travel_cost = _get_travel_cost(d['dest_data']['city']['country'])
            meal_cost = _get_meal_cost(d['dest_data']['city']['country'])

            d.update({
                'travel_cost': travel_cost,
                'meal_cost': meal_cost,
            })
            updated_results.append(d)
            print(d)

        data.update({'results': updated_results})

        return data        

    origin = request.args.get('origin', 'BKK')
    departure_date = request.args.get('departure_date', '2015-08-20')
    return_date = request.args.get('return_date', '2015-09-02')
    max_price = request.args.get('max_price', 200)




    data = flights.where_to_go(
        origin=origin,
        departure_date="{departure_date}--{return_date}".format(
            departure_date=departure_date, return_date=return_date),
        max_price=max_price
    )

    data = flights.include_dest_loc_data(data)
    
    data = _include_cost_data(data)

    return flask.jsonify(**data)

@app.route("/cheapest-rooms-radius")
def cheapest_rooms_radius():
    check_in = request.args.get('check_in', '2015-08-24')
    check_out = request.args.get('check_out', '2015-08-28')
    latitude = request.args.get('latitude', 16.44671)
    longitude = request.args.get('longitude', 102.833)
    currency = request.args.get('currency', 'USD')
    max_rate = request.args.get('max_rate', 100)

    data = hotels.cheapest_rooms_radius(
        check_in=check_in,
        check_out=check_out,
        latitude=latitude,
        longitude=longitude,
        currency=currency,
        max_rate=max_rate,
        radius=300
    )


    return flask.jsonify(**data)    

@app.route("/get-location")
def get_location():
    code = request.args.get('code', 'BKK')

    data = transport.get_location(code=code)
    return flask.jsonify(**data)    


@app.route("/when-to-go")
def when_to_go():
    data = flights.when_to_go(
        origin='NCE',
        destination='LAS',
        departure_date='2015-10-25--2016-01-30',
        duration='4--10'
    )
    return flask.jsonify(**data)


@app.route("/")
def main():
    print bucket
    return "Welcome!"

@app.route("/about")
def about():
    return "About page!"


@app.route("/hello")
def hello():
    return "Hello page!"

if __name__ == '__main__':
    app.run(debug=True)
