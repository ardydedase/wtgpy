import requests

from couchbase.bucket import Bucket
bucket = Bucket('couchbase://localhost/wheretogo')


API_URL = 'https://api.sandbox.amadeus.com/v1.2'
API_KEY = 'xx5nE3UgOfRAwCFw9pw2WAwKiWojqMRR'

STRICT, GRACEFUL, IGNORE = 'strict', 'graceful', 'ignore'

class Transport(object):
    def __init__(self, api_key=API_KEY, api_url=API_URL):
        self.api_key = API_KEY
        self.api_url = API_URL

    def make_request(self, service_url, method='get', headers=None, data=None,
                     callback=None, errors=STRICT, **params):
    
        params.update({'apikey': self.api_key})

        request = getattr(requests, method.lower())
        r = request(service_url, headers=headers, data=data, params=params)
        print(r.url)
        print(r)

        return r.json()

    def get_location(self, code='BKK'):
        loc_key = "loc-{code}".format(code=code)
        try:
            loc_data = bucket.get(loc_key)
        except Exception:
            print("can't get key")
            loc_data = {}

        if loc_data.value:
            print("found loc_data: %s" % loc_data.value)
            return loc_data.value

        path = "location/{code}".format(code=code)
        service_url =  "{url}/{path}".format(url=self.api_url, path=path)
        loc_data = self.make_request(service_url)
        
        bucket.insert(loc_key, loc_data)  
        print("saved key %s" % loc_key)

        return loc_data     


class Flights(Transport):
    def __init__(self, api_key=API_KEY, api_url=API_URL):
        self.api_url = "{api_url}/flights".format(api_url=api_url)
        self.api_key = api_key

    def include_dest_loc_data(self, data):
        updated_results = []
        print("loc_data")

        transport = Transport()

        for d in data['results']:
            print("\n")
            print("\n")
            loc_data = transport.get_location(d['destination'])            
            d.update({
                'dest_data': loc_data
            })

            updated_results.append(d)
            print(d)

        data.update({'results': updated_results})

        return data

    def where_to_go(self, **params):
        service_url =  "{url}/{path}".format(url=self.api_url, path='inspiration-search')
        return self.make_request(service_url, **params)

    def when_to_go(self, **params):
        service_url =  "{url}/{path}".format(url=self.api_url, path='extensive-search')
        return self.make_request(service_url, **params)

class Hotels(Transport):
    def __init__(self, api_key=API_KEY, api_url=API_URL):
        self.api_url = "{api_url}/hotels".format(api_url=api_url)
        self.api_key = api_key

    def cheapest_rooms_radius(self, **params):
        service_url =  "{url}/{path}".format(url=self.api_url, path='search-circle')
        return self.make_request(service_url, **params)

    def when_to_go(self, **params):
        service_url =  "{url}/{path}".format(url=self.api_url, path='extensive-search')
        return self.make_request(service_url, **params)        



