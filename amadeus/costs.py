MEAL_COST = {
    "Japan": {
        "low": 8.08,
        "medium": 24.23,
        "high": 71
	},
	"Brunei": {
        "low": 2.92,
        "medium": 18.23,
        "high": 50
	},
	"Cambodia": {
        "low": 3,
        "medium": 18,
        "high": 70
	},
	"Indonesia": {
        "low": 1.86,
        "medium": 11.14,
        "high": 55
	},
	"Laos": {
        "low": 2,
        "medium": 24.91,
        "high": 50
	},
	"Malaysia": {
        "low": 2.1,
        "medium": 13.12,
        "high": 79
	},
	"Myanmar": {
        "low": 3,
        "medium": 17.5,
        "high": 40
	},
	"Philippines": {
        "low": 2.2,
        "medium": 13.19,
        "high": 50
	},
	"Singapore": {
        "low": 7.29,
        "medium": 36.46,
        "high": 80
	},
	"Thailand": {
        "low": 1.43,
        "medium": 16.20,
        "high": 40
	},
	"Vietnam": {
        "low": 1.83,
        "medium": 13.75,
        "high": 40
	}
}

TRAVEL_COST = {
    "Japan": {
        "low": 12.88,
        "medium": 59.19,
        "high": 261.9
    },
    "Brunei": {
        "low": 8,
        "medium": 64,
        "high": 218
    },
    "Cambodia": {
        "low": 8,
        "medium": 79.2,
        "high": 149.6
    },
    "Indonesia": {
        "low": 2.4,
        "medium": 67,
        "high": 254
    },
    "Laos": {
        "low": 5.92,
        "medium": 50.53,
        "high": 106.7
    },
    "Malaysia": {
        "low": 4.16,
        "medium": 38.73,
        "high": 216.31
    },
    "Myanmar": {
        "low": 1.68,
        "medium": 25.6,
        "high": 385
    },
    "Philippines": {
        "low": 1.52,
        "medium": 51.57,
        "high": 210.51
    },
    "Singapore": {
        "low": 10.48,
        "medium": 109.93,
        "high": 118.35
    },
    "Thailand": {
        "low": 4.56,
        "medium": 24.32,
        "high": 318.08
    },
    "Vietnam": {
        "low": 2.56,
        "medium": 112.59,
        "high": 554
    }
}